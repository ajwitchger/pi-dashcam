Script changes

April 2016
	•	Added a second camera option using motion. The camera variables in the global.rc have changed to reflect this. The USB bus seems to struggle using a second USB camera, you may see problems. Check log files, dmesg etc. if you do.

January 2016
	•	Fixed the cleanup subroutine so it honours the 'usb_directory' variable.
	•	Added the home.sh script so files can be copied from the Pi to another location over WiFi.

November 2015 
	•	Changed recording file names to event#-sequence#_date
	•	Files can be copied from USB storage to the SD card automatically. Place them in a directory called scripts at the USB mount point and they will be moved to /root/scripts at startup.
	•	gps_logger GPS fix requirements are now much stricter.
	•	Added support for the UPS PIco.
	•	Fixed some minor bugs here and there.
	•	Included more things from the global.rc into the picam.py program. E.G. km/h or mp/h
	•	'pivid_enabled' has been replaced by 'pi_camera_method' for using pivid, picam or nothing.
	•	Even more options added to the global.rc!

June 2015 
	•	Fixed GPS log files not rotating.
	•	Added a Python Pi camera (picam) or raspivid (pivid) option called 'camera_method'. Using picam allows GPS annotation to video. Needs Python3 (and a lot more polishing).
	•	Pivid recording time changed from milliseconds to seconds in global.rc
	•	Added GPS text annotation option 'motion_GPS_annotate' for motion video
	•	A few small changes and fixes here and there, nothing significant.

18th December 2014
	•	Modified all scripts to check for a global.rc in the 'main_directory/usb' and if found use any of these variables as a priority.
	•	Added 'usb_device' variable in global.rc so a user defined USB device can be set.
	•	Aligned some of the variables in the scripts and fixed a few minor bugs.

18th November 2014
	•	USB storage can have it's own global.rc file. If the '*_use_usb' variable is set to 'yes' in the main_directory/global.rc file and USB storage is found it will check the main_directory/usb for a global.rc file. If found, those variables will take priority.

17th October 2014
	•	Fixed issue with GPS reporting incorrect time.

24th August 2014
	•	Added 'over_temp' option so the Pi will shutdown gracefully if it gets too hot.
	•	Alternative main and log directories can now be defined.
	•	Added log rotation options for the GPS and status files. If they are not set to append then a user defined number of files will be kept instead.
	•	Fixed some incorrectly formatted expressions in the shell scripts.

22nd May 2014
	•	Changed *_disabled to *_enabled in global.rc. For example, pivid_disabled is now pivid_enabled.
	•	Added GPS_append_log and status_append_log option in global.rc. If set to "yes" new data is added to the end of the logfile. If set to "no" the last file is moved away to *.old and a new log file is created.
	•	Added log_directory option in the global.rc so a user specified location instead of /home/camera/logs can be used.
	•	Sending a SIGUSR1 to pwr_butt will shutdown the Pi (useful for an ignition sensing program).
	•	Avergage speed, distance and time is written to the gps_log.txt file.
	•	Fixed the writing of shutdown information to status.txt which was being terminated to early.

